<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "sso_saml".
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'SSO SAML',
  'description' => 'login feusers via SAML-SSO',
  'category' => 'fe',
  'author' => 'Christian Baer',
  'author_email' => 'chr.baer@gmail.com',
  'state' => 'beta',
  'uploadfolder' => false,
  'createDirs' => '',
  'clearCacheOnLoad' => 0,
  'version' => '1.0.0',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '10.2.0-10.9.9',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
  'clearcacheonload' => false,
  'author_company' => NULL,
);
