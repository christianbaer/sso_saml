<?php
defined('TYPO3_MODE') || die('Access denied.');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addService(
// Extension Key
    $_EXTKEY,
    // Service type
    'auth',
    // Service key
    'tx_sso_saml',
    array(
        'title' => 'SSO SAML Authentication',
        'description' => 'Login fe-users via SAML-SSO',
        'subtype' => 'getUserFE,authUserFE',
        'available' => true,
        'priority' => 90,
        'quality' => 90,
        'os' => '',
        'exec' => '',
        'className' => \Christianbaer\SsoSaml\Service\SamlAuthenticationService::class
    )
);