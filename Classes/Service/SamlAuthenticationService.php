<?php
namespace Christianbaer\SsoSaml\Service;

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManager;
use \TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication;
use \TYPO3\CMS\Core\Database\ConnectionPool;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Authentication\AuthenticationService;
use TYPO3\CMS\Core\Authentication\AbstractAuthenticationService;
use TYPO3\CMS\Core\Context\Context;

class SamlAuthenticationService extends AbstractAuthenticationService
{
    /**
     * 200 - authenticated and no more checking needed
     */
    const STATUS_AUTHENTICATION_SUCCESS_BREAK = 200;
    /**
     * 0 - this service was the right one to authenticate the user but it failed
     */
    const STATUS_AUTHENTICATION_FAILURE_BREAK = 0;
    /**
     * 100 - just go on. User is not authenticated but there's still no reason to stop
     */
    const STATUS_AUTHENTICATION_FAILURE_CONTINUE = 100;
    private $mySSO = null;

    protected $connectionPool;

    protected $extConf;

    public function __construct(ConnectionPool $connectionPool) {
        $this->connectionPool = $connectionPool;
    }

    public function initExtConf(): bool
    {
        $this->extConf = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('sso_saml');

        if(!$this->extConf['pathToSimplesamlphp'])
        {
            die('missing pathToSimplesamlphp. please check extension-configuration: Admin Tool > Settings > Extension Configuration > sso_saml');
        }
        if(!$this->extConf['serviceProviderKey'])
        {
            die('missing serviceProviderKey. please check extension-configuration: Admin Tool > Settings > Extension Configuration > sso_saml');
        }
        if(!$this->extConf['feUsersPid'])
        {
            die('missing feUsersPid. please check extension-configuration: Admin Tool > Settings > Extension Configuration > sso_saml');
        }
        if(!$this->extConf['feUserGroupId'])
        {
            die('missing feUserGroupId. please check extension-configuration: Admin Tool > Settings > Extension Configuration > sso_saml');
        }

        return true;
    }




    public function getUser()
    {
        $this->initExtConf();

        require_once $this->extConf['pathToSimplesamlphp']. '/lib/_autoload.php';
        $this->mySSO = new \SimpleSAML\Auth\Simple($this->extConf['serviceProviderKey']);

        $user = false;
        $isAuthenticatedViaSSO = $this->mySSO->isAuthenticated();

        if(!$isAuthenticatedViaSSO){
            // start saml-login
            $this->mySSO->login();
        }

//        $session = \SimpleSAML\Session::getSessionFromRequest();
//        $session->cleanup();



//        // check if fe-user is logged in;
//        $context = GeneralUtility::makeInstance(Context::class);
//        // Checking if a user is logged in
//        $userIsLoggedIn = $context->getPropertyFromAspect('frontend.user', 'isLoggedIn');
//        if($userIsLoggedIn){
//            return true;
//        }


        // check login-state
        if ($isAuthenticatedViaSSO) {

            $attributes = $this->mySSO->getAttributes();
#            var_dump($attributes);

            $firstname = $attributes['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname'][0];
            $lastname = $attributes['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname'][0];

            $username = $this->mySSO->getAuthData('saml:sp:NameID')->getValue();

        } else {
            return [];
        }


        // check if user exists
        $querybuilder = $this->connectionPool->getQueryBuilderForTable('fe_users');
        $row = $querybuilder
            ->select('*')
            ->from('fe_users')
            ->where(
                $querybuilder->expr()->eq('username', $querybuilder->createNamedParameter($username))
            )
            ->execute()
            ->fetch();

        if ($row){
            // found user
            $user = array(
                'uid' => $row['uid'],
                'username' => $row['username'],
                'usergroup' => $row['usergroup'],
                'disable' => '0',
                'name' => $row['first_name'] . ' ' . $row['last_name'],
            );

        } else {
            // no user in DB, so create one
            $userForDb = array(
                'username' => $username,
                'password' => 'loremipsum',
                'first_name' => $firstname,
                'last_name' => $lastname,
                'pid' => $this->extConf['feUsersPid'],
                'usergroup' => $this->extConf['feUserGroupId']
            );
            $res = $querybuilder
                ->insert('fe_users')
                ->values($userForDb)
                ->execute();
            $lastInsertId = $querybuilder->getConnection()->lastInsertId();

            #var_dump('new User with uid: '.$lastInsertId);

            $user = array(
                'uid' => $lastInsertId,
                'username' => $userForDb['username'],
                'disable' => '0',
                'name' => $userForDb['first_name'] . ' ' . $userForDb['last_name'],
                'usergroup' => intval($this->extConf['feUserGroupId'])
            );


        }



        return $user;
    }

    public function authUser(array $user): int
    {
        return (is_array($user) && array_key_exists('username', $user) && $user['username']) ? self::STATUS_AUTHENTICATION_SUCCESS_BREAK : self::STATUS_AUTHENTICATION_FAILURE_BREAK;
    }



}